# Setup
- Install dependences

```shell
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

- Run migrations
```shell
./manage.py migrate
```

- Start app
```shell
./manage.py runserver
```

# Usage
L'application est par défaut, accessible à l'adresse [http://localhost:8000](http://localhost:8000).
La page d'acceuil présente les capsules. Un bouton `Voir plus` permet de voir plus d'informations 
sur chaque capsule.

# Captures
- Acceuil

![Acceuil](./captures/capture0.png)

- Page de detail

![Detail](./captures/capture1.png)

