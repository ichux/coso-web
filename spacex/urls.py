from django.urls import path

from . import views

urlpatterns = [
    path("", views.capsules, name="capsules"),
    path("<str:id>", views.capsule, name="capsule"),
]
