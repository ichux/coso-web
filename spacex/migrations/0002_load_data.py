# Generated by Django 4.2 on 2023-04-20 14:30

import requests
from django.db import migrations


def load_capsules(apps, schema_editor):
    Capsule = apps.get_model("spacex", "Capsule")
    
    response = requests.get("https://api.spacexdata.com/v3/dragons")
    response.raise_for_status()
    
    Capsule.objects.using(schema_editor.connection.alias).bulk_create(
        [Capsule(**capsule) for capsule in response.json()]
    )


def unload_capsules(apps, schema_editor):
    apps.get_model("spacex", "Capsule").objects.all().delete()


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("spacex", "0001_initial"),
    ]

    operations = [migrations.RunPython(load_capsules, unload_capsules)]
