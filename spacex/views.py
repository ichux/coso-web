from django.shortcuts import get_object_or_404, render

from spacex.models import Capsule


def capsules(request):
    return render(request, "spacex/capsules.html", {"capsules": Capsule.objects.all()})


def capsule(request, id):
    return render(
        request, "spacex/capsule.html", {"capsule": get_object_or_404(Capsule, id=id)}
    )
